﻿// Mid Term.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "Mid Term.h"
#include <windowsX.h>
#include <winuser.h>
#include <fstream>
#include <locale>
#include <codecvt>
#include <iostream>
#include <string>
#include <vector>
#include <commctrl.h>
#pragma comment(linker,"\"/manifestdependency:type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")
#pragma comment(lib, "ComCtl32.lib")
using namespace std;

#define MAX_LOADSTRING 100

// Global Variables:
HINSTANCE hInst;                                // current instance
WCHAR szTitle[MAX_LOADSTRING];                  // The title bar text
WCHAR szWindowClass[MAX_LOADSTRING];            // the main window class name
TCHAR listItem1[256];
// Forward declarations of functions included in this code module:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);

struct Item {
	WCHAR LoaiChiTieu[20];
	WCHAR ThuChi[5];
	unsigned long long SoTien;
	WCHAR NoiDung[1024];
};


// Biến toàn cục

#define FILE_PATH		L"data.txt"

int listViewCount = 0;
unsigned long long TongTienChi = 0;
unsigned long long TongTienThu = 0;
unsigned long long TongTien_AnUong = 0;
unsigned long long TongTien_DiChuyen = 0;
unsigned long long TongTien_NhaCua = 0;
unsigned long long TongTien_XeCo = 0;
unsigned long long TongTien_NhuYeuPham = 0;
unsigned long long TongTien_DichVu = 0;
HWND cBox_LoaiChiTieu;
HWND edit_NoiDung;
HWND edit_SoTien;
HWND edit_TongTienChi;
HWND edit_TongTienThu;
HWND static_Khung1;
HWND static_Khung2;
HWND static_Khung3;
HWND static_TongTien;
HWND button_Them;
HWND static_Anuong;
HWND static_Dichuyen;
HWND static_Nhacua;
HWND static_Xeco;
HWND static_Nhuyeupham;
HWND static_Dichvu;
HWND listView;

bool isDraw = false;
bool loadFile = false;
bool isReset = false;
vector<Item*> DanhSachChiTieu;
// Danh sách cách hàm
HWND taoListView(HWND parentWnd, long ID, HINSTANCE hParentInst, int x, int y, int nWidth, int nHeight);
void themNoiDungVaoListView(HWND hWnd, int listViewCount);
void veBieuDoThongKe(HDC hdc);
void LuuFile(wstring path);
void LoadFile(wstring path);
void LoadDuLieuVaoListView();
void ResetBang(HWND hWnd);


int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    // TODO: Place code here.

    // Initialize global strings
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadStringW(hInstance, IDC_MIDTERM, szWindowClass, MAX_LOADSTRING);
    MyRegisterClass(hInstance);

    // Perform application initialization:
    if (!InitInstance (hInstance, nCmdShow))
    {
        return FALSE;
    }

    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_MIDTERM));

    MSG msg;

    // Main message loop:
    while (GetMessage(&msg, nullptr, 0, 0))
    {
        if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }

    return (int) msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEXW wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style          = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc    = WndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_MIDTERM));
    wcex.hCursor        = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground  = (HBRUSH)(COLOR_BTNFACE+1);
    wcex.lpszMenuName   = MAKEINTRESOURCEW(IDC_MIDTERM);
    wcex.lpszClassName  = szWindowClass;
    wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

    return RegisterClassExW(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   hInst = hInstance; // Store instance handle in our global variable

   HWND hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, nullptr, nullptr, hInstance, nullptr);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND  - process the application menu
//  WM_PAINT    - Paint the main window
//  WM_DESTROY  - post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	HWND other;
    switch (message)
    {
	case WM_CREATE: {

		INITCOMMONCONTROLSEX icc;
		icc.dwSize = sizeof(icc);
		icc.dwICC = ICC_WIN95_CLASSES;
		InitCommonControlsEx(&icc);

		// Lấy font hệ thống
		LOGFONT lf;
		GetObject(GetStockObject(DEFAULT_GUI_FONT), sizeof(LOGFONT), &lf);
		HFONT hFont = CreateFont(lf.lfHeight - 6, lf.lfWidth,
			lf.lfEscapement, lf.lfOrientation, lf.lfWeight,
			lf.lfItalic, lf.lfUnderline, lf.lfStrikeOut, lf.lfCharSet,
			lf.lfOutPrecision, lf.lfClipPrecision, lf.lfQuality,
			lf.lfPitchAndFamily, lf.lfFaceName);

		// Khung thứ nhất
		//===============================================================================================================================================================================
		static_Khung1 = CreateWindow(L"STATIC", L"Thêm một chi tiêu mới", WS_CHILD | WS_VISIBLE | ES_AUTOHSCROLL, 50, 30, 180, 30, hWnd, NULL, hInst, NULL);
		SendMessage(static_Khung1, WM_SETFONT, WPARAM(hFont), TRUE);

		// Tạo combobox loại chi tiêu
		cBox_LoaiChiTieu = CreateWindow(L"COMBOBOX", L"", WS_CHILD | WS_VISIBLE | CBS_DROPDOWNLIST | CBS_HASSTRINGS | WS_OVERLAPPED, 50, 100, 150, 100, hWnd, NULL, hInst, NULL);
		SendMessage(cBox_LoaiChiTieu, WM_SETFONT, WPARAM(hFont), TRUE);

		// Thêm vào combobox loại chi tiêu
		TCHAR *buffer = new TCHAR[1024];
		wsprintf(buffer, L"Thu nhập");
		SendMessage(cBox_LoaiChiTieu, CB_INSERTSTRING, 0, (LPARAM)buffer);
		wsprintf(buffer, L"Dịch vụ");
		SendMessage(cBox_LoaiChiTieu, CB_INSERTSTRING, 0, (LPARAM)buffer);
		wsprintf(buffer, L"Nhu yếu phẩm");
		SendMessage(cBox_LoaiChiTieu, CB_INSERTSTRING, 0, (LPARAM)buffer);
		wsprintf(buffer, L"Xe cộ");
		SendMessage(cBox_LoaiChiTieu, CB_INSERTSTRING, 0, (LPARAM)buffer);
		wsprintf(buffer, L"Nhà cửa");
		SendMessage(cBox_LoaiChiTieu, CB_INSERTSTRING, 0, (LPARAM)buffer);
		wsprintf(buffer, L"Di chuyển");
		SendMessage(cBox_LoaiChiTieu, CB_INSERTSTRING, 0, (LPARAM)buffer);
		wsprintf(buffer, L"Ăn uống");
		SendMessage(cBox_LoaiChiTieu, CB_INSERTSTRING, 0, (LPARAM)buffer);

		RECT rect_LoaiChiTieu;
		GetWindowRect(cBox_LoaiChiTieu, &rect_LoaiChiTieu);

		// Tạo các đề mục
		other = CreateWindow(L"STATIC", L"Loại chi tiêu:", WS_CHILD | WS_VISIBLE | SS_LEFT, 50, 70, 150, rect_LoaiChiTieu.bottom - rect_LoaiChiTieu.top, hWnd, NULL, hInst, NULL);
		SendMessage(other, WM_SETFONT, WPARAM(hFont), TRUE);
		other = CreateWindow(L"STATIC", L"Số tiền:", WS_CHILD | WS_VISIBLE | SS_LEFT, 250, 70, 150, rect_LoaiChiTieu.bottom - rect_LoaiChiTieu.top, hWnd, NULL, hInst, NULL);
		SendMessage(other, WM_SETFONT, WPARAM(hFont), TRUE);
		other = CreateWindow(L"STATIC", L"Nội dung:", WS_CHILD | WS_VISIBLE | SS_LEFT, 450, 70, 150, rect_LoaiChiTieu.bottom - rect_LoaiChiTieu.top, hWnd, NULL, hInst, NULL);
		SendMessage(other, WM_SETFONT, WPARAM(hFont), TRUE);

		// Tạo khung nhập số tiền
		edit_SoTien = CreateWindow(L"EDIT", L"", WS_CHILD | WS_VISIBLE | WS_BORDER | ES_AUTOHSCROLL, 250, 100, 150, rect_LoaiChiTieu.bottom-rect_LoaiChiTieu.top, hWnd, NULL, hInst, NULL);
		SendMessage(edit_SoTien, WM_SETFONT, WPARAM(hFont), TRUE);

		// Tạo khung nhập nội dung
		edit_NoiDung = CreateWindow(L"EDIT", L"", WS_CHILD | WS_VISIBLE | WS_BORDER | ES_AUTOHSCROLL, 450, 100, 350, rect_LoaiChiTieu.bottom - rect_LoaiChiTieu.top, hWnd, NULL, hInst, NULL);
		SendMessage(edit_NoiDung, WM_SETFONT, WPARAM(hFont), TRUE);
		
		// Tạo Button Thêm
		button_Them = CreateWindow(L"BUTTON", L"THÊM", WS_CHILD | WS_VISIBLE | BS_DEFPUSHBUTTON, 850, 100, 100, rect_LoaiChiTieu.bottom - rect_LoaiChiTieu.top, hWnd, (HMENU)IDC_BUTTON_THEM, hInst, NULL);
		SendMessage(button_Them, WM_SETFONT, WPARAM(hFont), TRUE);

		//===============================================================================================================================================================================

		// Khung thứ 2
		//===============================================================================================================================================================================
		static_Khung2 = CreateWindow(L"STATIC", L"Danh sách các chi tiêu", WS_CHILD | WS_VISIBLE | ES_AUTOHSCROLL, 50, 180, 180, 30, hWnd, NULL, hInst, NULL);
		SendMessage(static_Khung2, WM_SETFONT, WPARAM(hFont), TRUE);
		// Tạo listView để thể hiện danh sách
		listView = taoListView(hWnd, ID_LISTVIEW, hInst, 50, 230, 900, 250);
		//===============================================================================================================================================================================

		// Khung thứ 3
		//===============================================================================================================================================================================
		static_Khung3 = CreateWindow(L"STATIC", L"Thống kê số tiền đã dùng", WS_CHILD | WS_VISIBLE | ES_AUTOHSCROLL, 50, 540, 200, 30, hWnd, NULL, hInst, NULL);
		SendMessage(static_Khung3, WM_SETFONT, WPARAM(hFont), TRUE);

		other = CreateWindow(L"STATIC", L"Tổng tiền chi: ", WS_CHILD | WS_VISIBLE | SS_LEFT, 50, 580, 150, rect_LoaiChiTieu.bottom - rect_LoaiChiTieu.top, hWnd, NULL, hInst, NULL);
		SendMessage(other, WM_SETFONT, WPARAM(hFont), TRUE);
		other = CreateWindow(L"STATIC", L"Tiền thu nhập: ", WS_CHILD | WS_VISIBLE | SS_LEFT, 350, 580, 150, rect_LoaiChiTieu.bottom - rect_LoaiChiTieu.top, hWnd, NULL, hInst, NULL);
		SendMessage(other, WM_SETFONT, WPARAM(hFont), TRUE);

		// Tạo edit tổng tiền
		edit_TongTienChi = CreateWindow(L"EDIT", L"", WS_CHILD | WS_VISIBLE | WS_BORDER | ES_AUTOHSCROLL | ES_READONLY, 170, 580, 150, rect_LoaiChiTieu.bottom - rect_LoaiChiTieu.top, hWnd, NULL, hInst, NULL);
		SendMessage(edit_TongTienChi, WM_SETFONT, WPARAM(hFont), TRUE);
		edit_TongTienThu = CreateWindow(L"EDIT", L"", WS_CHILD | WS_VISIBLE | WS_BORDER | ES_AUTOHSCROLL | ES_READONLY, 470, 580, 150, rect_LoaiChiTieu.bottom - rect_LoaiChiTieu.top, hWnd, NULL, hInst, NULL);
		SendMessage(edit_TongTienChi, WM_SETFONT, WPARAM(hFont), TRUE);
		//===============================================================================================================================================================================
		// Load Dữ liệu cũ
		LoadFile(FILE_PATH);
		LoadDuLieuVaoListView();
		// chú thích

		other = CreateWindow(L"STATIC", L"Ăn uống", WS_CHILD | WS_VISIBLE | SS_LEFT, 1170, 265, 110, rect_LoaiChiTieu.bottom - rect_LoaiChiTieu.top, hWnd, NULL, hInst, NULL);
		SendMessage(other, WM_SETFONT, WPARAM(hFont), TRUE);
		other = CreateWindow(L"STATIC", L"Di chuyển", WS_CHILD | WS_VISIBLE | SS_LEFT, 1170, 335, 110, rect_LoaiChiTieu.bottom - rect_LoaiChiTieu.top, hWnd, NULL, hInst, NULL);
		SendMessage(other, WM_SETFONT, WPARAM(hFont), TRUE);
		other = CreateWindow(L"STATIC", L"Nhà cửa", WS_CHILD | WS_VISIBLE | SS_LEFT, 1170, 405, 110, rect_LoaiChiTieu.bottom - rect_LoaiChiTieu.top, hWnd, NULL, hInst, NULL);
		SendMessage(other, WM_SETFONT, WPARAM(hFont), TRUE);
		other = CreateWindow(L"STATIC", L"Xe cộ", WS_CHILD | WS_VISIBLE | SS_LEFT, 1170, 475, 110, rect_LoaiChiTieu.bottom - rect_LoaiChiTieu.top, hWnd, NULL, hInst, NULL);
		SendMessage(other, WM_SETFONT, WPARAM(hFont), TRUE);
		other = CreateWindow(L"STATIC", L"Nhu yếu phẩm", WS_CHILD | WS_VISIBLE | SS_LEFT, 1170, 545, 110, rect_LoaiChiTieu.bottom - rect_LoaiChiTieu.top, hWnd, NULL, hInst, NULL);
		SendMessage(other, WM_SETFONT, WPARAM(hFont), TRUE);
		other = CreateWindow(L"STATIC", L"Dịch vụ", WS_CHILD | WS_VISIBLE | SS_LEFT, 1170, 615, 110, rect_LoaiChiTieu.bottom - rect_LoaiChiTieu.top, hWnd, NULL, hInst, NULL);
		SendMessage(other, WM_SETFONT, WPARAM(hFont), TRUE);

		// Tạo nhóm %
		static_Anuong = CreateWindow(L"STATIC", L"", WS_CHILD | WS_VISIBLE | SS_CENTER, 1300, 265, 95, rect_LoaiChiTieu.bottom - rect_LoaiChiTieu.top, hWnd, NULL, hInst, NULL);
		SendMessage(static_Anuong, WM_SETFONT, WPARAM(hFont), TRUE);
		static_Dichuyen = CreateWindow(L"STATIC", L"", WS_CHILD | WS_VISIBLE | SS_CENTER, 1300, 335, 95, rect_LoaiChiTieu.bottom - rect_LoaiChiTieu.top, hWnd, NULL, hInst, NULL);
		SendMessage(static_Dichuyen, WM_SETFONT, WPARAM(hFont), TRUE);
		static_Nhacua = CreateWindow(L"STATIC", L"", WS_CHILD | WS_VISIBLE | SS_CENTER, 1300, 405, 95, rect_LoaiChiTieu.bottom - rect_LoaiChiTieu.top, hWnd, NULL, hInst, NULL);
		SendMessage(static_Nhacua, WM_SETFONT, WPARAM(hFont), TRUE);
		static_Xeco = CreateWindow(L"STATIC", L"", WS_CHILD | WS_VISIBLE | SS_CENTER, 1300, 475, 95, rect_LoaiChiTieu.bottom - rect_LoaiChiTieu.top, hWnd, NULL, hInst, NULL);
		SendMessage(static_Xeco, WM_SETFONT, WPARAM(hFont), TRUE);
		static_Nhuyeupham = CreateWindow(L"STATIC", L"", WS_CHILD | WS_VISIBLE | SS_CENTER, 1300, 545, 95, rect_LoaiChiTieu.bottom - rect_LoaiChiTieu.top, hWnd, NULL, hInst, NULL);
		SendMessage(static_Nhuyeupham, WM_SETFONT, WPARAM(hFont), TRUE);
		static_Dichvu = CreateWindow(L"STATIC", L"", WS_CHILD | WS_VISIBLE | SS_CENTER, 1300, 615, 95, rect_LoaiChiTieu.bottom - rect_LoaiChiTieu.top, hWnd, NULL, hInst, NULL);
		SendMessage(static_Dichvu, WM_SETFONT, WPARAM(hFont), TRUE);

		// Tiêu đề
		GetObject(GetStockObject(DEFAULT_GUI_FONT), sizeof(LOGFONT), &lf);
		HFONT hFont1 = CreateFont(lf.lfHeight - 40, lf.lfWidth - 30,
			lf.lfEscapement, lf.lfOrientation, lf.lfWeight,
			lf.lfItalic, lf.lfUnderline, lf.lfStrikeOut, lf.lfCharSet,
			lf.lfOutPrecision, lf.lfClipPrecision, lf.lfQuality,
			lf.lfPitchAndFamily, lf.lfFaceName);
		other = CreateWindow(L"STATIC", L"QUẢN LÝ CHI TIÊU", WS_CHILD | WS_VISIBLE | SS_CENTER, 1000, 50, 400, 150, hWnd, NULL, hInst, NULL);
		SendMessage(other, WM_SETFONT, WPARAM(hFont1), TRUE);
	}break;
    case WM_COMMAND:
        {
			
            int wmId = LOWORD(wParam);
            // Parse the menu selections:

            switch (wmId)
            {
            case IDM_ABOUT:
                DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
                break;
            case IDM_EXIT:
                DestroyWindow(hWnd);
                break;
			case IDC_BUTTON_THEM: {
				themNoiDungVaoListView(hWnd, listViewCount);
			}break;
			case ID_MENU_NEW: {
				ResetBang(hWnd);
			}break;
			case ID_MENU_SAVE: {
				LuuFile(FILE_PATH);
			}break;
            default:
                return DefWindowProc(hWnd, message, wParam, lParam);
            }
        }
        break;
    case WM_PAINT:
        {
            PAINTSTRUCT ps;
            HDC hdc = BeginPaint(hWnd, &ps);
			veBieuDoThongKe(hdc);

			HPEN pen = CreatePen(PS_DOT, 1, RGB(0, 0, 0));
			SelectObject(hdc, pen);

			// Vẽ khung cho HWND khung1
			MoveToEx(hdc, 100, 50, NULL);
			LineTo(hdc, 970, 50);
			MoveToEx(hdc, 20, 50, NULL);
			LineTo(hdc, 40, 50);
			MoveToEx(hdc, 20, 150, NULL);
			LineTo(hdc, 970, 150);
			MoveToEx(hdc, 20, 50, NULL);
			LineTo(hdc, 20, 150);
			MoveToEx(hdc, 970, 50, NULL);
			LineTo(hdc, 970, 150);

			// Vẽ khung cho HWND khung2
			MoveToEx(hdc, 100, 200, NULL);
			LineTo(hdc, 970, 200);
			MoveToEx(hdc, 20, 200, NULL);
			LineTo(hdc, 40, 200);
			MoveToEx(hdc, 20, 510, NULL);
			LineTo(hdc, 970, 510);
			MoveToEx(hdc, 970, 200, NULL);
			LineTo(hdc, 970, 510);
			MoveToEx(hdc, 20, 200, NULL);
			LineTo(hdc, 20, 510);

			// Vẽ khung cho HWND khung3
			MoveToEx(hdc, 100, 560, NULL);
			LineTo(hdc, 970, 560);
			MoveToEx(hdc, 20, 560, NULL);
			LineTo(hdc, 40, 560);
			MoveToEx(hdc, 20, 700, NULL);
			LineTo(hdc, 970, 700);
			MoveToEx(hdc, 20, 560, NULL);
			LineTo(hdc, 20, 700);
			MoveToEx(hdc, 970, 560, NULL);
			LineTo(hdc, 970, 700);

			// Vẽ khung chú thích %
			MoveToEx(hdc, 1000, 200, NULL);
			LineTo(hdc, 1400, 200);
			MoveToEx(hdc, 1000, 200, NULL);
			LineTo(hdc, 1000, 700);
			MoveToEx(hdc, 1400, 200, NULL);
			LineTo(hdc, 1400, 700);
			MoveToEx(hdc, 1000, 700, NULL);
			LineTo(hdc, 1400, 700);


			// Chú thích ăn uống
			RECT *rect = new RECT;
			rect->left = 1050;
			rect->top = 250;
			rect->right = 1150;
			rect->bottom = 300;
			HBRUSH brush = CreateSolidBrush(RGB(178, 0, 31));
			FillRect(hdc, rect, brush);

			// Chú thích di chuyển
			
			rect->left = 1050;
			rect->top = 320;
			rect->right = 1150;
			rect->bottom = 370;
			brush = CreateSolidBrush(RGB(236, 135, 14));
			FillRect(hdc, rect, brush);

			// Chú thích nhà cửa
			rect->left = 1050;
			rect->top = 390;
			rect->right = 1150;
			rect->bottom = 440;
			brush = CreateSolidBrush(RGB(252, 245, 76));
			FillRect(hdc, rect, brush);

			// chú thích xe cộ
			rect->left = 1050;
			rect->top = 460;
			rect->right = 1150;
			rect->bottom = 510;
			brush = CreateSolidBrush(RGB(91, 189, 43));
			FillRect(hdc, rect, brush);

			// chú thích nhu yếu phẩm
			rect->left = 1050;
			rect->top = 530;
			rect->right = 1150;
			rect->bottom = 580;
			brush = CreateSolidBrush(RGB(31, 90, 167));
			FillRect(hdc, rect, brush);

			// chú thích dịch vụ
			rect->left = 1050;
			rect->top = 600;
			rect->right = 1150;
			rect->bottom = 650;
			brush = CreateSolidBrush(RGB(81, 31, 144));
			FillRect(hdc, rect, brush);

            EndPaint(hWnd, &ps);
        }
        break;
    case WM_DESTROY:
		LuuFile(FILE_PATH);
        PostQuitMessage(0);
        break;
    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }
    return 0;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    UNREFERENCED_PARAMETER(lParam);
    switch (message)
    {
    case WM_INITDIALOG:
        return (INT_PTR)TRUE;

    case WM_COMMAND:
        if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
        {
            EndDialog(hDlg, LOWORD(wParam));
            return (INT_PTR)TRUE;
        }
        break;
    }
    return (INT_PTR)FALSE;
}


HWND taoListView(HWND parentWnd, long ID, HINSTANCE hParentInst, int x, int y, int nWidth, int nHeight) {
	HWND m_listView = CreateWindow(WC_LISTVIEWW, L"", WS_CHILD | WS_VISIBLE | WS_VSCROLL | LVS_REPORT | WS_BORDER | WS_HSCROLL, x, y, nWidth, nHeight, parentWnd, (HMENU)ID, hParentInst, NULL);
	INITCOMMONCONTROLSEX icex;

	// Ensure that the common control DLL is loaded
	icex.dwSize = sizeof(INITCOMMONCONTROLSEX);
	icex.dwICC = ICC_LISTVIEW_CLASSES | ICC_TREEVIEW_CLASSES;
	InitCommonControlsEx(&icex);

	LVCOLUMN lvCol;
	lvCol.mask = LVCF_TEXT | LVCF_FMT | LVCF_WIDTH;
	lvCol.fmt = LVCFMT_CENTER;
	// cột 1;
	lvCol.cx = 150;
	lvCol.pszText = L"Loại";
	ListView_InsertColumn(m_listView, 0, &lvCol);
	// cột 2;
	lvCol.cx = 100;
	lvCol.pszText = L"Thu/chi";
	ListView_InsertColumn(m_listView, 1, &lvCol);
	// cột 3;
	lvCol.fmt = LVCFMT_LEFT;
	lvCol.cx = 150;
	lvCol.pszText = L"Số tiền";
	ListView_InsertColumn(m_listView, 2, &lvCol);
	// Cột 4
	lvCol.fmt = LVCFMT_LEFT;
	lvCol.cx = 500;
	lvCol.pszText = L"Nội dung";
	ListView_InsertColumn(m_listView, 3, &lvCol);
	return m_listView;
}

void themNoiDungVaoListView(HWND hWnd, int listViewCount) {
	Item* x = new Item();
	
	int len;
	// Lấy nội dung từ cBox_LoaiChiTieu
	len = GetWindowTextLength(cBox_LoaiChiTieu);
	if (len > 0) {
		GetWindowText(cBox_LoaiChiTieu, x->LoaiChiTieu, len + 1);
	}
	else {
		MessageBox(0, L"Loại chi tiêu chưa được chọn", L"Thông báo", 0);
		return;
	}
	
	// Gán nội dung cho thu chi
	if (wcscmp(x->LoaiChiTieu, L"Thu nhập") == 0) {
		
		wsprintf(x->ThuChi, L"Thu");
	}
	else
		wsprintf(x->ThuChi, L"Chi");

	// Lấy nội dung từ edit_SoTien
	len = GetWindowTextLength(edit_SoTien);
	if (len > 0) {
		WCHAR*  buffer = new WCHAR[len + 1];
		GetWindowText(edit_SoTien, buffer, len + 1);
		for (int i = 0; i < wcslen(buffer); i++) {
			if (buffer[i]<'0' || buffer[i]>'9') {
				SetWindowText(edit_SoTien, L"");
				SetWindowText(edit_NoiDung, L"");
				MessageBox(0, L"Số tiền chỉ nhấp nhận ký tự là số", L"Thông báo", 0);
				return;
			}
		}
		x->SoTien = _wtoi64(buffer);
	}
	else {
		MessageBox(0, L"Số tiền chưa được nhập", L"Thông báo", 0);
		return;
	}

	// Lấy nội dung từ edit_NoiDung
	len = GetWindowTextLength(edit_NoiDung);
	if (len > 0) {
		GetWindowText(edit_NoiDung, x->NoiDung, len + 1);
		
	}
	else {
		MessageBox(0, L"Nội dung chưa được nhập", L"Thông báo", 0);
		return;
	}


	if (x->ThuChi[0] == 'T') {
		if (wcscmp(x->LoaiChiTieu, L"Thu nhập") == 0) {
			TongTienThu = TongTienThu + x->SoTien;
			WCHAR buffer_TongTien[20];
			wsprintf(buffer_TongTien, L"%I64d", TongTienThu);
			SetWindowText(edit_TongTienThu, buffer_TongTien);
		}
		else {
			MessageBox(0, L"Chọn sai mục", 0, 0);
			SetWindowText(edit_SoTien, L"");
			SetWindowText(edit_NoiDung, L"");
			return;
		}
	}
	else {
		TongTienChi = TongTienChi + x->SoTien;
		WCHAR buffer_TongTien[20];
		wsprintf(buffer_TongTien, L"%I64d", TongTienChi);
		SetWindowText(edit_TongTienChi, buffer_TongTien);

		if (wcscmp(x->LoaiChiTieu, L"Ăn uống") == 0) {
			TongTien_AnUong = TongTien_AnUong + x->SoTien;
		}
		if (wcscmp(x->LoaiChiTieu, L"Di chuyển") == 0) {
			TongTien_DiChuyen = TongTien_DiChuyen + x->SoTien;
		}
		if (wcscmp(x->LoaiChiTieu, L"Nhà cửa") == 0) {
			TongTien_NhaCua = TongTien_NhaCua + x->SoTien;
		}
		if (wcscmp(x->LoaiChiTieu, L"Xe cộ") == 0) {
			TongTien_XeCo = TongTien_XeCo + x->SoTien;
		}
		if (wcscmp(x->LoaiChiTieu, L"Nhu yếu phẩm") == 0) {
			TongTien_NhuYeuPham = TongTien_NhuYeuPham + x->SoTien;
		}
		if (wcscmp(x->LoaiChiTieu, L"Dịch vụ") == 0) {
			TongTien_DichVu = TongTien_DichVu + x->SoTien;
		}
		
	}

	isDraw = true;
	LV_ITEM lv;
	lv.mask = LVIF_TEXT | LVIF_PARAM;

	// Thêm vào cột Loại chi tiêu
	lv.iItem = listViewCount;
	lv.iSubItem = 0;
	lv.pszText = x->LoaiChiTieu;
	ListView_InsertItem(listView, &lv);

	lv.mask = LVIF_TEXT;
	// Thêm vào cột thu chi
	lv.iSubItem = 1;
	lv.pszText = x->ThuChi;
	ListView_SetItem(listView, &lv);

	// Thêm vào cột tiền
	lv.iSubItem = 2;
	WCHAR*  buffer = new WCHAR[20];
	wsprintf(buffer, L"%I64d", x->SoTien);
	lv.pszText = buffer;
	ListView_SetItem(listView, &lv);

	// Thêm vào cột nội dung
	lv.iSubItem = 3;
	lv.pszText = x->NoiDung;
	ListView_SetItem(listView, &lv);

	SetWindowText(edit_SoTien, L"");
	SetWindowText(edit_NoiDung, L"");

	DanhSachChiTieu.push_back(x);

	listViewCount++;

	RedrawWindow(hWnd, NULL, NULL, RDW_INTERNALPAINT | RDW_UPDATENOW | RDW_ERASENOW | RDW_INVALIDATE);
}

void veBieuDoThongKe(HDC hdc) {
	
	float TienAnUong;
	float TienDiChuyen;
	float TienXeCo;
	float TienNhaCua;
	float TienNhuYeuPham;
	float TienDichVu;

	if (TongTienChi == 0) {
		Rectangle(hdc, 50, 630, 950, 675);
		RECT *rect = new RECT;
		rect->left = 50;
		rect->top = 630;
		rect->right = 950;
		rect->bottom = 675;
		HBRUSH brush = CreateSolidBrush(RGB(0, 0, 0));
		FillRect(hdc, rect, brush);
		SetWindowText(static_Anuong, L"0%");
		SetWindowText(static_Dichuyen, L"0%"); 
		SetWindowText(static_Nhacua, L"0%");
		SetWindowText(static_Xeco, L"0%"); 
		SetWindowText(static_Nhuyeupham, L"0%");
		SetWindowText(static_Dichvu, L"0%");

		return;
	}
	else {
		if (TongTien_AnUong == 0)
			TienAnUong = 0;
		else
			TienAnUong = (float)TongTien_AnUong / (float)TongTienChi;

		if (TongTien_DiChuyen == 0)
			TienDiChuyen = 0;
		else
			TienDiChuyen = (float)TongTien_DiChuyen / (float)TongTienChi;

		if (TongTien_XeCo == 0)
			TienXeCo = 0;
		else
			TienXeCo = (float)TongTien_XeCo / (float)TongTienChi;

		if (TongTien_NhaCua == 0)
			TienNhaCua = 0;
		else
			TienNhaCua = (float)TongTien_NhaCua / (float)TongTienChi;

		if (TongTien_NhuYeuPham == 0)
			TienNhuYeuPham = 0;
		else
			TienNhuYeuPham = (float)TongTien_NhuYeuPham / (float)TongTienChi;


		if (TongTien_DichVu == 0)
			TienDichVu = 0;
		else
			TienDichVu = (float)TongTien_DichVu / (float)TongTienChi;
	}
	
	WCHAR buffer[20];
	unsigned long long a = (int)(TienAnUong * 100);
	wsprintf(buffer, L"%I64d%", a);
	wcscat_s(buffer, L"%");
	SetWindowText(static_Anuong, buffer);

	a = (int)(TienDiChuyen * 100);
	wsprintf(buffer, L"%I64d", a);
	wcscat_s(buffer, L"%");
	SetWindowText(static_Dichuyen, buffer);

	a = (int)(TienNhaCua * 100);
	wsprintf(buffer, L"%I64d", a);
	wcscat_s(buffer, L"%");
	SetWindowText(static_Nhacua, buffer);

	a = (int)(TienXeCo * 100);
	wsprintf(buffer, L"%I64d", a);
	wcscat_s(buffer, L"%");
	SetWindowText(static_Xeco, buffer);

	a = (int)(TienNhuYeuPham * 100);
	wsprintf(buffer, L"%I64d", a);
	wcscat_s(buffer, L"%");
	SetWindowText(static_Nhuyeupham, buffer);

	a = (int)(TienDichVu * 100);
	wsprintf(buffer, L"%I64d", a);
	wcscat_s(buffer, L"%");
	SetWindowText(static_Dichvu, buffer);

	int x_standard = 50;
	// Vẽ Ăn uống
	if (isDraw == true || loadFile == true || isReset == false) {

		if (TienAnUong != 0) {
			int x = TienAnUong * 900;
			Rectangle(hdc, x_standard, 630, x_standard + x, 675);
			RECT *rect = new RECT;
			rect->left = x_standard;
			rect->top = 630;
			rect->right = x_standard + x;
			rect->bottom = 675;
			HBRUSH brush = CreateSolidBrush(RGB(178, 0, 31));
			FillRect(hdc, rect, brush);
			x_standard = x_standard + x;
		}

		if (TienDiChuyen != 0) {
			int x = TienDiChuyen * 900;
			Rectangle(hdc, x_standard, 630, x_standard + x, 675);
			RECT *rect = new RECT;
			rect->left = x_standard;
			rect->top = 630;
			rect->right = x_standard + x;
			rect->bottom = 675;
			HBRUSH brush = CreateSolidBrush(RGB(236, 135, 14));
			FillRect(hdc, rect, brush);
			x_standard = x_standard + x;
		}

		if (TienNhaCua != 0) {
			int x =  TienNhaCua* 900;
			Rectangle(hdc, x_standard, 630, x_standard + x, 675);
			RECT *rect = new RECT;
			rect->left = x_standard;
			rect->top = 630;
			rect->right = x_standard + x;
			rect->bottom = 675;
			HBRUSH brush = CreateSolidBrush(RGB(252, 245, 76));
			FillRect(hdc, rect, brush);
			x_standard = x_standard + x;
		}

		if (TienXeCo != 0) {
			int x = TienXeCo * 900;
			Rectangle(hdc, x_standard, 630, x_standard + x, 675);
			RECT *rect = new RECT;
			rect->left = x_standard;
			rect->top = 630;
			rect->right = x_standard + x;
			rect->bottom = 675;
			HBRUSH brush = CreateSolidBrush(RGB(91, 189, 43));
			FillRect(hdc, rect, brush);
			x_standard = x_standard + x;
		}

		if (TienNhuYeuPham != 0) {
			int x = TienNhuYeuPham * 900;
			Rectangle(hdc, x_standard, 630, x_standard + x, 675);
			RECT *rect = new RECT;
			rect->left = x_standard;
			rect->top = 630;
			rect->right = x_standard + x;
			rect->bottom = 675;
			HBRUSH brush = CreateSolidBrush(RGB(31, 90, 167));
			FillRect(hdc, rect, brush);
			x_standard = x_standard + x;
		}

		if (TienDichVu != 0) {
			int x = TienDichVu * 900;
			Rectangle(hdc, x_standard, 630, x_standard + x, 675);
			RECT *rect = new RECT;
			rect->left = x_standard;
			rect->top = 630;
			rect->right = x_standard + x;
			rect->bottom = 675;
			HBRUSH brush = CreateSolidBrush(RGB(81, 31, 144));
			FillRect(hdc, rect, brush);
			x_standard = x_standard + x;
		}
	}
	if(isReset == true && isDraw == false && loadFile == false){
		Rectangle(hdc, 50, 630, 950, 675);
		RECT *rect = new RECT;
		rect->left = 50;
		rect->top = 630;
		rect->right =  950;
		rect->bottom = 675;
		HBRUSH brush = CreateSolidBrush(RGB(0, 0, 0));
		FillRect(hdc, rect, brush);
	}
	
}



void LuuFile(wstring path) {
	const locale utf8_locale = locale(locale(), new codecvt_utf8<wchar_t>());
	std::wofstream f(path);
	f.imbue(utf8_locale);

	f << TongTienChi << endl;
	f << TongTienThu << endl;

	for (int i = 0; i < DanhSachChiTieu.size(); i++) {
		f << wstring(DanhSachChiTieu[i]->LoaiChiTieu) << endl;
		f << wstring(DanhSachChiTieu[i]->ThuChi) << endl;
		f << DanhSachChiTieu[i]->SoTien << endl;
		f << wstring(DanhSachChiTieu[i]->NoiDung) << endl;
	}
	f.close();
}





void LoadFile(wstring path) {
	const locale utf8_locale = locale(locale(), new codecvt_utf8<wchar_t > ());
	wfstream f;
	f.imbue(utf8_locale);
	f.open(path, ios::in);
	loadFile = true;
	wstring buffer;
	if (f.is_open()) {

		if (getline(f, buffer))
			TongTienChi = _wtoi64(buffer.c_str());
		if (getline(f, buffer))
			TongTienThu = _wtoi64(buffer.c_str());

		while (getline(f, buffer)) {
			Item* x = new Item();
			WCHAR buffer_LoaiChiTieu[30];
			WCHAR buffer_ThuChi[5];
			WCHAR buffer_NoiDung[1024];

			wcscpy_s(x->LoaiChiTieu, buffer.c_str());

			getline(f, buffer);

			wcscpy_s(x->ThuChi, buffer.c_str());

			getline(f, buffer);

			x->SoTien = _wtoi64(buffer.c_str());

			getline(f, buffer);

			wcscpy_s(x->NoiDung, buffer.c_str());

			DanhSachChiTieu.push_back(x);

			if (wcscmp(x->LoaiChiTieu, L"Ăn uống") == 0)
				TongTien_AnUong += x->SoTien;
			if (wcscmp(x->LoaiChiTieu, L"Di chuyển") == 0) {
				TongTien_DiChuyen = TongTien_DiChuyen + x->SoTien;
			}
			if (wcscmp(x->LoaiChiTieu, L"Nhà cửa") == 0) {
				TongTien_NhaCua = TongTien_NhaCua + x->SoTien;
			}
			if (wcscmp(x->LoaiChiTieu, L"Xe cộ") == 0) {
				TongTien_XeCo = TongTien_XeCo + x->SoTien;
			}
			if (wcscmp(x->LoaiChiTieu, L"Nhu yếu phẩm") == 0) {
				TongTien_NhuYeuPham = TongTien_NhuYeuPham + x->SoTien;
			}
			if (wcscmp(x->LoaiChiTieu, L"Dịch vụ") == 0) {
				TongTien_DichVu = TongTien_DichVu + x->SoTien;
			}

		}

		
	}
	f.close();
}

void LoadDuLieuVaoListView() {
	LV_ITEM lv;
	WCHAR* buffer = new WCHAR[20];

	for (int i = 0; i < DanhSachChiTieu.size(); i++) {
		lv.mask = LVIF_TEXT;

		lv.iItem = i;
		lv.iSubItem = 0;
		lv.pszText = DanhSachChiTieu[i]->LoaiChiTieu;
		ListView_InsertItem(listView, &lv);

		lv.mask = LVIF_TEXT;
		lv.iSubItem = 1;
		lv.pszText = DanhSachChiTieu[i]->ThuChi;
		ListView_SetItem(listView, &lv);
		if (DanhSachChiTieu[i]->SoTien != 0) {
			lv.iSubItem = 2;
			wsprintf(buffer, L"%I64d", DanhSachChiTieu[i]->SoTien);
			lv.pszText = buffer;
			ListView_SetItem(listView, &lv);
		}
		

		lv.iSubItem = 3;
		lv.pszText = DanhSachChiTieu[i]->NoiDung;
		ListView_SetItem(listView, &lv);
	}
}

void ResetBang(HWND hWnd) {
	DanhSachChiTieu.clear();
	ListView_DeleteAllItems(listView);
	TongTienChi = 0;
	TongTienThu = 0;
	TongTien_AnUong = 0;
	TongTien_DiChuyen = 0;
	TongTien_NhaCua = 0;
	TongTien_XeCo = 0;
	TongTien_DichVu = 0;
	TongTien_NhuYeuPham = 0;
	isDraw = false;
	isReset = true;
	loadFile = false;
	SetWindowText(edit_TongTienThu, L"");
	SetWindowText(edit_TongTienChi, L"");
	RedrawWindow(hWnd, NULL, NULL, RDW_INTERNALPAINT | RDW_UPDATENOW | RDW_ERASENOW | RDW_INVALIDATE);
}