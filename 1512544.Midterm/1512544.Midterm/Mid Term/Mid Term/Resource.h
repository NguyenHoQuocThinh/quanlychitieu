//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by Mid Term.rc
//
#define IDC_MYICON                      2
#define IDD_MIDTERM_DIALOG              102
#define IDS_APP_TITLE                   103
#define IDD_ABOUTBOX                    103
#define IDM_ABOUT                       104
#define IDM_EXIT                        105
#define IDI_MIDTERM                     107
#define IDI_SMALL                       108
#define IDC_MIDTERM                     109
#define ID_THEM                         110
#define IDC_BUTTON_THEM                 111
#define ID_LISTVIEW                     112
#define ID_RESET                        113
#define IDR_MAINFRAME                   128
#define ID_MENU_NEW                     32771
#define ID_MENU_SAVE                    32772
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        129
#define _APS_NEXT_COMMAND_VALUE         32773
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           114
#endif
#endif
