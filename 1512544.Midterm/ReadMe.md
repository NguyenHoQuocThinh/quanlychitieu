﻿# Đại học khoa học tự nhiên
# -------------------------------------------------
###Họ tên: NGUYỄN HỒ QUỐC THỊNH
###MSSV 1512544
# -------------------------------------------------
#Chức năng đã hoàn thành:
1. Cho người dùng nhập loại chi tiêu
2. Tính tổng tất cả chi tiêu
3. Phân loại theo biểu đồ dạng hình chữ nhật nằm ngang
4. Từng loại chi tiêu sẽ mang một màu riêng biệt
5. Có bảng thống kê theo phần trăm giữa các loại chi tiêu
#Luồng sự kiện chính:
* Người dùng chọn loại chi tiêu và nhập số tiền cùng nội dung
* Khi người dùng tắt chương trình thì chương trình sẽ tự động lưu lại những dữ liệu của người dùng
* Khi mở chương trình lên thì chương trình sẽ tự động load những dữ liệu lúc trước của người dùng
#Luồng sự kiện phụ:
*
#Link bitbucket: https://NguyenHoQuocThinh@bitbucket.org/NguyenHoQuocThinh/quanlychitieu
#Link video demo hướng dẫn: https://youtu.be/_FCo9NZRC-U
